import { Component } from "react";

let today = new Date();
let hours = today.getHours();
let minutes = today.getMinutes();
let seconds = today.getSeconds();
let halfDay = hours < 12 ? 'AM' : 'PM';

class TimeNow extends Component {
     constructor(props) {
          super(props);
          this.state = {
               color: ''
          }
     }

     onBtnChangeColor = () => {
          if (seconds % 2 === 0) {
               this.setState({
                    color: 'red'
               })
          } else {
               this.setState({
                    color: 'blue'
               })
          }
     }

     render() {
          return (
               <div>
                    <h1><b>Hello, world!</b></h1>
                    <h3><b style={{ color: this.state.color }}>It is {hours}:{minutes}:{seconds} {halfDay}.</b></h3>
                    <button onClick={this.onBtnChangeColor}>Change Color</button>
               </div>
          )
     }
}

export default TimeNow;
