import logo from './logo.svg';
import './App.css';
import TimeNow from './components/TimeNow';

function App() {
  return (
    <div>
      <TimeNow/>
    </div>
  );
}

export default App;
